package com.twuc.webApp.web;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class BookController {

    @GetMapping("/users/{id}/books")
    public String getBookInformation(@PathVariable Long id) {
        return "The book for user " + id;
    }

    @GetMapping("/segments/good")
    public String getSegment() {
        return "this is good segment";
    }

    @GetMapping("/segments/{segmentName}")
    public String getSegmentName(@PathVariable String segmentName) {
        return "this is bad " + segmentName;
    }

    @GetMapping("/?/hello")
    public String hello() {
        return "hello world";
    }

    @GetMapping("/wildcards/*")
    public String wildcardAtEndOfPath() {
        return "this wildcards at end of path";
    }

    @GetMapping("/*/wildcards")
    public String wildcardInPath() {
        return "this wildcards in path";
    }

    @GetMapping("/wildcard/before/*/after")
    public String matchPathWithWildcard() {
        return "this method match all path";
    }

    @GetMapping("/wildcards/a*b/book")
    public String matchPathWithSuffix() {
        return "this is a book";
    }

    @GetMapping("/**/book")
    public String matchPathWithMultipleWildcards() {
        return "this is the book";
    }

    @GetMapping("/user/book/{id:\\d+}")
    public String matchPathWithCanonical(@PathVariable("id") Long id) {
        return "this is the canonical " + id;
    }
}
