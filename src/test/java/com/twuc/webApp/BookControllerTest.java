package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_book_information_when_parameter_is_id() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("The book for user 2"));
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("The book for user 23"));
    }

    @Test
    void should_throw_exception_when_parameter_is_not_long() throws Exception {
        mockMvc.perform(get("/api/users/s/books"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_call_the_accurate_method() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(status().isOk())
                .andExpect(content().string("this is good segment"));
    }

    @Test
    void should_return_the_error_code_when_give_multiple_parameters() throws Exception {
        mockMvc.perform(get("/api/aaaa/hello"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_the_erroe_code_when_not_give_parameter() throws Exception {
        mockMvc.perform(get("/api/hello"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_match_all_path() throws Exception {
        mockMvc.perform(get("/api/wildcards/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("this wildcards at end of path"));

        mockMvc.perform(get("/api/wildcards/12"))
                .andExpect(status().isOk())
                .andExpect(content().string("this wildcards at end of path"));
    }

    @Test
    void should_match_all_path_with_wildcard() throws Exception {
        mockMvc.perform(get("/api/1/wildcards"))
                .andExpect(status().isOk())
                .andExpect(content().string("this wildcards in path"));

        mockMvc.perform(get("/api/2/wildcards"))
                .andExpect(status().isOk())
                .andExpect(content().string("this wildcards in path"));

    }

    @Test
    void should_throw_error_code_with_wildcard() throws Exception {
        mockMvc.perform(get("/api/before/after"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_the_book_information_when_give_prefix_and_suffix() throws Exception {
        mockMvc.perform(get("/api/wildcards/acb/book"))
                .andExpect(status().isOk())
                .andExpect(content().string("this is a book"));
    }

    @Test
    void should_return_the_book_information_when_give_multiple_wildcards() throws Exception {
        mockMvc.perform(get("/api/aaa/book"))
                .andExpect(status().isOk())
                .andExpect(content().string("this is the book"));
    }

    @Test
    void should_return_the_information_when_give_Canonical() throws Exception {
        mockMvc.perform(get("/api/user/book/2"))
                .andExpect(status().isOk())
                .andExpect(content().string("this is the canonical 2"));
        mockMvc.perform(get("/api/user/book/ss"))
                .andExpect(status().isNotFound());

    }


}
